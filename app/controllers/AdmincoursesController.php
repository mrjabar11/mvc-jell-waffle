<?php
namespace App\Controllers;
use Core\Controller;
use App\Models\Courses;


class AdmincoursesController extends Controller {
  public function __construct($controller, $action){
    parent::__construct($controller, $action);
    $this->view->setLayout('admin');
  }


  public function indexAction(){
    $this->view->render('admincourses/index');
  }

  public function addAction(){
    $course = new Courses();
    if($this->request->isPost()){
      $this->request->csrfCheck();
      $course->assign($this->request->get());
      $course->save();
    }

    $this->view->course = $course;
    $this->view->formAction = PROOT . 'admincourses/add';
    $this->view->displayErrors = $course->getErrorMessages();
    $this->view->render('admincourses/add');
  }
}
