<?php
namespace App\Models;
use Core\Model;
use Core\Validators\RequiredValidator;
use Core\Validators\NumericValidator;



  class Courses extends Model {

    public $id, $created_at, $updated_at, $title, $description, $price, $list, $deleted = 0;

    public function __construct(){
      $table = 'courses';
      parent::__construct($table);
    }

    public function beforeSave(){
      $this->timeStamps();
    }

    public function validator(){
      $requiredFields = ['title'=>'Title', 'description'=>'Description','price'=>'Price', 'list'=>'List Price'];
      foreach($requiredFields as $field => $display){
        $this->runValidation(new RequiredValidator($this, ['field'=>$field, 'msg'=>$display." is required."]));
      }
      $this->runValidation(new NumericValidator($this, ['field'=>'price', 'msg'=>'Price must be a number.']));
      $this->runValidation(new NumericValidator($this, ['field'=>'list', 'msg'=>'List Price must be a number.']));

    }
}
