<?php $this->setSiteTitle('Add Course')?>
<?php $this->start('body')?>
  <h1>Add New Course</h1>
  <div class="row">
    <div class="col-md-10 col-md-offset-1 well">
      <?php $this->partial('admincourses', 'form')?>
    </div>
  </div>
<?php $this->end()?>
