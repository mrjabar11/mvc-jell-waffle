<?php
use Core\FH;
?>
<form action="<?=$this->formAction?>" method="POST" enctype="multipart/form-data">
  <?= FH::csrfInput();?>
  <?= FH::displayErrors($this->displayErrors) ?>
  <?= FH::inputBlock('text', 'Title', 'title', $this->course->title,['class'=>'form-control input-sm'],['class'=>'form-group col-md-6']) ?>
  <?= FH::textareaBlock('Description', 'description', $this->course->description,['class'=>'form-control input-sm'],['class'=>'form-group col-md-6']) ?>
  <?= FH::inputBlock('text', 'Price', 'price', $this->course->price,['class'=>'form-control input-sm'],['class'=>'form-group col-md-6']) ?>
  <?= FH::inputBlock('text', 'List Price', 'list', $this->course->list,['class'=>'form-control input-sm'],['class'=>'form-group col-md-6']) ?>

  <?= FH::submitBlock('Save',['class'=>'btn btn-large btn-primary'], ['class'=>'text-right col-md-12']) ?>

</form>
